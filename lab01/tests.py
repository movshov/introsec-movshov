import os
import sys
import pytest
import pexpect

def verify_file_exists(filename):
  assert os.path.exists(filename)

def test_oracle_compiled():
  verify_file_exists("./oracle")

def test_answers_exists():
  verify_file_exists("./answers.txt")

def test_player_compiled():
  verify_file_exists("./player")

def test_guesses_exists():
  verify_file_exists("./guesses.txt")


def run_oracle(name, number, desired_result):
  cmd = "./oracle"
  child = pexpect.spawn(cmd)

  assert child.expect_exact("What is your name?", timeout=1) == 0
  child.sendline(name)

  assert child.expect_exact("What is the magic number, %s?" % name, timeout=1) == 0
  child.sendline(str(number))

  status_codes = ["TOO LOW", "SUCCESS", "TOO HIGH"]
  result = child.expect(status_codes, timeout=1)
  assert status_codes[result] == desired_result


def test_oracle_bob00():
  run_oracle("Bob", 0, "TOO LOW")

def test_oracle_bob01():
  run_oracle("Bob", 1, "TOO LOW")

def test_oracle_bob02():
  run_oracle("Bob", 2, "SUCCESS")

def test_oracle_bob03():
  run_oracle("Bob", 3, "TOO HIGH")

def test_oracle_bob04():
  run_oracle("Bob", 2**17-1, "TOO HIGH")

def test_oracle_bob05():
  run_oracle("Bob", -2**17, "TOO LOW")

def test_oracle_longname_toolow():
  run_oracle("ThisIsQuiteAVeryVeryLongNameIsntIt", 99999, "TOO LOW")

def test_oracle_longname_success():
  run_oracle("ThisIsQuiteAVeryVeryLongNameIsntIt", 999999, "SUCCESS")

def test_oracle_longname_toohigh():
  run_oracle("ThisIsQuiteAVeryVeryLongNameIsntIt", 9999999, "TOO HIGH")

def test_oracle_longname_negative():
  run_oracle("ThisIsQuiteAVeryVeryLongNameIsntIt", -999999, "TOO LOW")


def run_player(name, guess, response):
  cmd = "./player %s" % name
  child = pexpect.spawn(cmd)

  child.sendline("What is your name?")
  assert child.expect_exact(name, timeout=1) == 0

  child.sendline("What is the magic number, %s?" % name)
  assert child.expect_exact(guess, timeout=1) == 0

  child.sendline(response)


def test_player_bob():
  run_player("Bob", "22", "SUCCESS")

def test_player_alice():
  run_player("Alice", "1", "SUCCESS")

def test_player_carol():
  run_player("Carol", "3", "SUCCESS")

