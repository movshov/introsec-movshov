# CS 491/591 Intro to Computer Security #

This repo contains instructions and skeleton code for your programming labs.

## Homework Assignments ##
* [Lab 0: Getting Started](lab00/README.md)
* [Lab 1: C Programming: A Refresher](lab01/README.md)
* [Lab 2: The Layout of a Program in Memory](lab02/README.md)
* [Lab 3: Smashing the Stack](lab03/README.md)
* Lab 4: Password Authentication
