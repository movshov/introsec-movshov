
import os
from Cryptodome.Hash import SHA256
import subprocess
import random

def sha256(msg):
  return SHA256.new(msg).hexdigest()

known_hashes = {}


def load_hashes():
  hashes = {}
  hash_filename = "hashes.txt"
  for line in open(hash_filename, 'r'):
    toks = line.strip().split()
    if len(toks) != 2: continue
    h = toks[0]
    msg = toks[1]
    hashes[msg] = h
  return hashes


def get_hash(msg):
  global known_hashes
  if msg not in known_hashes:
    known_hashes = load_hashes()
  return known_hashes[msg]


def load_flags(flag_filename):
  flags = {}
  for line in open(flag_filename, 'r'):
    #print("Read line [%s]" % line.strip())
    toks = line.strip().split()
    if len(toks) != 2:
      continue
    if not "FLAG" in toks[0]:
      continue
    flag_id = toks[0]
    the_flag = line
    flags[flag_id] = the_flag
  #print("Found flags =", flags)
  return flags


def verify_flag(flag, flagnum):
  # First a sanity check for proper formatting
  # The flag should look something like "FLAG abcd1234..."
  toks = flag.split()
  if len(toks) < 2:
    raise AssertionError("Didn't find a flag")
  if "FLAG" not in toks[0]:
    raise AssertionError("Didn't find a flag")
  # Second, make sure we're consistent with newlines.
  # When the flag comes from a file, it'll be there.
  # It's easiest if we ensure that this is always the case.
  # So when it's missing, we just add it here before the hash.
  if not flag.endswith("\n"):
    flag += "\n"
  # Load the correct hash from our hashes file
  filename = "flag%02d.txt" % flagnum
  real_flags = load_flags(filename)
  # The two hashes should be identical
  if flag != real_flags["FLAG"]:
    raise AssertionError("Hashes don't match!  [%s] vs [%s]" % (my_hash, h))

def randomize_flag_file(flagnum):
  flag_filename = "flag%02d.txt" % flagnum
  new_flag = random.randint(0,2**31-1)
  with open(flag_filename, 'w') as f:
    f.write("FLAG 0x%08x\n" % new_flag)


def verify_flag_dynamic(flagnum):

  # Dynamically generate a new flag each time we test
  randomize_flag_file(flagnum)

  target_program = "./target%02d" % flagnum
  cmd = "./attack%02d | %s" % (flagnum, target_program)

  p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
  stdout_output, stderr_output = p.communicate()
  stdout_lines = stdout_output.split(b'\n')
  flag = None
  for bline in stdout_lines:
    line = bline.decode('utf8')
    if line.startswith('FLAG'):
      flag = line
      verify_flag(flag, flagnum)
  if flag is None:
    raise AssertionError("Didn't find a flag")


# Check the saved flags 
# They should be stored in files named flag??.txt

# Removing these for Spring 2020
#def test_flag01():
#  verify_flag_file(1)
#
#def test_flag02():
#  verify_flag_file(2)
#
#def test_flag03():
#  verify_flag_file(3)
#
#def test_flag04():
#  verify_flag_file(4)
#
#def test_flag05():
#  verify_flag_file(5)



# Check the code that gets the flag for each problem
# Each file should define a get_flag() function

#def test_prob01():
#  verify_flag_dynamic(1)
#
#def test_prob02():
#  verify_flag_dynamic(2)
#
#def test_prob03():
#  verify_flag_dynamic(3)
#
#def test_prob04():
#  verify_flag_dynamic(4)
#
#def test_prob07():
#  verify_flag_dynamic(7)
#
#def test_prob08():
#  verify_flag_dynamic(8)
#
#def test_prob09():
#  verify_flag_dynamic(9)


