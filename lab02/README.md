# Lab 2: The Layout of a Program in Memory #

You can think of this lab as "half programming exercise, half science experiment."  You are given a simple C program that allocates various kinds of resources in its memory space.  You will make some simple modifications to the program code in order to observe where in the program's memory each class of resource is allocated.

## Getting Started: Make sure you have the latest version of the assignment ##
Use Git to pull the latest updates from the ```introsec-public``` repo, which your local working copy knows as "origin".

```console
user@host:lab02$ git pull origin master
```


## Getting Started: Disabling ASLR ##
For the first several steps in this lab, we also need to tell the operating system to temporarily disable address space layout randomization (ASLR).  This will make it easier to see the patterns in the addresses across multiple runs of the program.

```console
user@host:lab02$ sudo sysctl -w kernel.randomize_va_space=0
```

## The Baseline Program ##
You can find the starting code for this lab in [baseline.c](baseline.c).  It's a simple program with two global variables, two functions, and a handful of local variables in each function.  Its code is shown in its entirety below.

```C
#include <stdio.h>
#include <malloc.h>

int A;
int B;

int function(int depth) {
    int rc;
    char buf[5];
    char *stuff = (char *) malloc(16*sizeof(char));

    if(depth < 10)
        rc = function(depth+1);
    else
        rc = 0;

    free(stuff);

    return rc;
}

int main(int argc, char* argv[]) {
    int x;
    char *buffer = (char *) malloc(128*sizeof(char));
    int  *array = (int *) malloc(128*sizeof(int));

    x = function(10);

    return 0;
}
```

Let's go ahead and compile and run this program.  From the ```lab02``` directory, first run ```make``` to compile the program.  Then you can execute the compiled executable as ```./baseline```.

```console
user@host:lab02$ make
gcc -o baseline baseline.c
user@host:lab02$ ./baseline
user@host:lab02$ 
```

Hmmm.  Our program ran, but we didn't get any output.  We'll address that in the steps below.

## Step 1: Observing Stack Allocations ##
Your first task is to add some instrumentation code so we can see where the local variables are allocated in ```main```.

### Task 1a: Adding Our First Instrumentation ###
The file [tracer1a.c](tracer1a.c) contains a copy of the baseline code.  Modify it so that it prints the addresses of the local variables ```x``` in ```main```, like this:

```console
user@host:lab02$ make
gcc -o baseline baseline.c
gcc -o tracer1a tracer1a.c
user@host:lab02$ ./tracer1a
x = 0xff8fec20
```

How did we do that?  It can be as simple as adding a single call to ```printf``` in ```main```, like this:

```C
int main(int argc, char* argv[]) {
    int x;
    char *buffer = (char *) malloc(128*sizeof(char));
    int  *array = (int *) malloc(128*sizeof(int));

    printf("x = %p\n", &x);     // Add this line!

    x = function(10);

    return 0;
}
```

Here, we're using the ```%p``` format specifier to tell ```printf``` that the value we want to print is a **pointer**.  ```printf``` prints the value of the pointer in hexadecimal notation, complete with the nice ```0x``` prefix for us.

Also notice that the argument we passed to ```printf``` was not ```x``` itself but *the address of* ```x```, written in C as ```&x```.

After making the change described above, save your [tracer1a.c](tracer1a.c) and turn it in via git.  Remember, it takes three commands to complete this process: **git add**, then **git commit**, then **git push**.

```console
user@host:lab02$ git add tracer1a.c
user@host:lab02$ git commit -m "Finished the code for tracer 1a"
[master 3e89de8] Finished the code for tracer 1a
 1 files changed, 2 insertions(+), 0 deletions(-)
user@host:lab02$ git push personal master
Counting objects: 9, done.
Compressing objects: 100% (9/9), done.
Writing objects: 100% (9/9), 1.96 KiB | 1.96 MiB/s, done.
Total 1 (delta 1), reused 0 (delta 0)
To bitbucket.org:youraccount/introsec-you.git
   cf2f16f..3e89de8  master -> master
```

### Task 1b: Instrumenting the Other Stack Variables ###

Next you will complete the process of adding instrumentation for your program's stack.  

First, make a copy of your ```tracer1a.c``` in ```tracer1b.c```:

```console
user@host:lab02$ cp tracer1a.c tracer1b.c
```

Next, edit the file to add similar outputs for the following list of local variables:

* ```rc``` in ```function```
* ```buf``` in ```function```

When you're done, submit your code for [tracer1b.c](tracer1b.c) in git.


### Question 1 ###
Q: Run ```tracer1b``` a few times and observe the results.  Are the memory locations for these three variables in any certain order?  In other words, are some of the variables consistently stored at higher (or lower) addresses than the others?  What's going on here and why?

Submit your answer in [answers01.md](answers01.md).


## Step 2: Observing Heap Allocations ##
For this part, start by copying your previous code into a new file [tracer2.c](tracer2.c).

```console
user@host:lab02$ cp tracer1b.c tracer2.c
```

Your next step is to add a few lines of instrumentation code so we can see the addresses for the variables that are allocated on the heap by ```malloc```.

Specifically, these are:

* ```buffer``` and ```array``` in ```main```
* ```stuff``` in ```function```

Hint: When you instrument ```function```, it's easier if you put your calls to ```printf``` before the recursive call back to ```function```.  If you do it the other way around, you'll have to think extra hard to answer the next question.

When you're done, submit your code for [tracer2.c](tracer2.c) in git with the usual sequence (**git add**, then **git commit**, then **git push personal master**).

### Question 2 ###
Q: Compile and run ```tracer2``` a few times and observe the results.

1. Where are ```buffer``` and ```array``` relative to ```stuff```?
2. What happens to the address of ```stuff``` each time the function recurses?
3. What is going on here?  Explain why we get these behaviors in (1) and (2) above.

Submit your answer in [answers02.md](answers02.md).

## Step 3: Observing Code Locations ##

For the final step, make one last copy of your code.

```console
user@host:lab02$ cp tracer2.c tracer3.c
```

Now add some instrumentation code to print out the addresses of the following functions:

* ```main```
* ```function```
* ```printf```

### Question 3 ###
Compile and run ```tracer3``` and observe the results.

1. Each time you run the program, what do all of the code addresses have in common?
2. Why does (1) happen?  Explain what you're seeing.
3. Now re-run the program several times.  Do the addresses change from run to run?  If so, *how* do they change each time?

Submit your answers in [answers03.md](answers03.md).

## One More Step: Re-enabling ASLR ##
Now we'll turn ASLR back on and observe the results.

```console
user@host:lab02$ sudo sysctl -w kernel.randomize_va_space=2
```

### Question 4 ###
Now that ASLR is re-enabled, run ```tracer3``` again several times and see if you can tell what's happening.

1. How many bits is your system ASLR randomizing for the stack?  How can you tell?  Pick one of the stack variables and report the highest address that you observed for that variable, and the lowest address for the same variable.
2. How many bits of randomization are being applied to your heap?  As above, give the highest and lowest addresses that you observed for one of the heap variables.
3. How many bits for the code?  As above, report the highest and lowest addresses that you observed for a function in the program.

Submit your answers in [answers04.md](answers04.md).

## Turning in your code ##
Finally, please remember to submit your code and your answers to the questions into your Bitbucket repo where we can fetch it for grading.

```console
user@host:lab02$ git add tracer1a.c tracer1b.c tracer2.c tracer3.c answers01.md answers02.md answers03.md answers04.md
user@host:lab02$ git commit -m "Last commit for Lab 2"
user@host:lab02$ git push personal master
```
