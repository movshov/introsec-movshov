#include <stdio.h>
#include <malloc.h>

int A;
int B;

int function(int depth) {
    int rc;
    char buf[5];
    char *stuff = (char *) malloc(16*sizeof(char));

    printf("stuff = %p\n",&stuff);

    if(depth < 10)
        rc = function(depth+1);
	else
		rc = 0;

	free(stuff);

    printf("rc = %p\n",&rc);
    printf("buf = %p\n",&buf);
    return rc;
}

int main(int argc, char* argv[]) {
    int x;
    char *buffer = (char *) malloc(128*sizeof(char));
    int  *array = (int *) malloc(128*sizeof(int));
    printf("buffer = %p\n",&buffer);
    printf("array = %p\n",&array);
    printf("x = %p\n",&x);  //print address of x as pointer.

    x = function(7);

    return 0;
}
