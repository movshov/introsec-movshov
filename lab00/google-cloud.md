# Creating Your Virtual Machine on GCE #

## Claiming Your Free Credits ##
If you have not yet received your free promotional Google Cloud credits, click on [cloud.google.com/freetrial](https://cloud.google.com/freetrial) to create a Google Cloud account and get the free credits.  NOTE: Be sure to use your **@pdx.edu** account when you do this!

## Creating Your Project in GCP ##
Log in to [console.cloud.google.com](https://console.cloud.google.com/) using your pdx.edu account.  At the top of the page, next to the "Google Cloud Platform" title, you'll see the name of the currently selected project.  It may say "PDX.EDU" -- if it does, click it and this will bring up a dialog box where you can create a new project for use in this course.  (The big "PDX.EDU" project belongs to PSU's IT group, and regular users like you and me don't have access to it.)  Pick an informative name for your project -- maybe something like "Intro to Computer Security" or "introsec".

## Redeeming Your Educational Credits ##
Once you have a Google Cloud account, you can click the link in the email from me to also receive $50 additional credits from Google for use in this course.  If you have multiple active projects in GCP, be sure to apply your new $50 to the project that you created for this class.

## Creating Your Virtual Machine ##
From your Google Cloud Platform dashboard at [console.cloud.google.com](https://console.cloud.google.com/), click the "hamburger" menu at the top left of the page to expand the menu.  You will see a long list of Google's cloud platform services.  Under the "COMPUTE" section of the menu, click on "Compute Engine," then "Virtual Machines."  At this point, Google cloud will probably pause for a short while as it initializes Compute Engine for your project.  (If it doesn't let you go to Compute Engine, double-check that you are looking at your own GCP project, and not at the main "PDX.EDU" one that we cannot access.)

From the Compute Engine page, click "Create Instance".  ([Direct Link](https://console.cloud.google.com/compute/instancesAdd))  This will bring up a form for you to fill out with all the information for your new Virtual machine.

* Name: It's your VM, you can call it whatever you like.  Sticking with something short will keep your shell prompt readable.  E.g. "introsec" or "cs491" or "cs591".
* Region: Please select "us-west1 (Oregon)"
* Zone: Doesn't matter for our purposes.  (I seem to always get us-west1-b)
* Machine Type: Select "g1-small (1 vCPU; 1.7 GB memory)" for a VM that will run for 3 months on your $50 educational credits.
* Boot Disk: Click "Change", to bring up a new menu to choose a new OS.  Then select:
    - Operating System: Ubuntu
    - Version: Ubuntu 18.04 LTS	
    - Click the "Select" button to accept these choices and return to the VM creation menu.
* Finally, click "Create" to create your new Ubuntu g1-small VM.

Your browser will go back to the VM Instances page, and you will have to wait a few seconds while Compute Engine creates your VM.  Your VM will appear in your list of virtual machines, but until the VM is ready, you'll have only a spinning "wait" icon in place of its status information.

## Logging in to Your New VM ##
Back on the "VM Instances" page, your previously-empty list of virtual machines should now show your new VM in the list.

Click "SSH" in the rightmost column ("Connect") and your browser will launch a new window and begin a new SSH session with your VM.  It will take a few seconds to transfer keys and do some setup, then you will be presented with a new Linux terminal where you're logged in to your new VM.  Your username will be your "ODIN ID", aka the first part of your PDX email address -- the part that comes before "@pdx.edu".

From there, you can use your VM just like any other Linux machine.  Your user is an administrator, and you can use the "sudo" command without a password.

Let's take this opportunity to install any missing security patches or other updates from Ubuntu, using the ```apt``` tool for managing software on the Ubuntu OS.

```console
user@introsec:~$ sudo apt update
Hit:1 http://us-west1.gce.archive.ubuntu.com/ubuntu bionic InRelease
Get:2 http://us-west1.gce.archive.ubuntu.com/ubuntu bionic-updates InRelea
se [88.7 kB]
Get:3 http://us-west1.gce.archive.ubuntu.com/ubuntu bionic-backports InRel
ease [74.6 kB]
Hit:4 http://archive.canonical.com/ubuntu bionic InRelease               
Get:5 http://security.ubuntu.com/ubuntu bionic-security InRelease [88.7 kB
]
Fetched 252 kB in 1s (316 kB/s)                              
Reading package lists... Done
Building dependency tree       
Reading state information... Done
14 packages can be upgraded. Run 'apt list --upgradable' to see them.
user@introsec:~$ sudo apt upgrade
Reading package lists... Done
Building dependency tree       
Reading state information... Done
Calculating upgrade... Done
The following packages were automatically installed and are no longer required:
  grub-pc-bin libdumbnet1 libnuma1 linux-gcp-headers-5.0.0-1020
  linux-gcp-headers-5.0.0-1021 linux-gcp-headers-5.0.0-1025
  linux-gcp-headers-5.0.0-1028 linux-gcp-headers-5.0.0-1029
Use 'sudo apt autoremove' to remove them.
The following packages will be upgraded:
  apport gcc-8-base libatomic1 libcc1-0 libgcc1 libgomp1 libitm1
  liblsan0 libmpx2 libquadmath0 libstdc++6 libtsan0 python3-apport
  python3-problem-report
14 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.
Need to get 217 kB/1396 kB of archives.
After this operation, 1024 B of additional disk space will be used.
Do you want to continue? [Y/n]
```

Of course, when you run this command, the list of packages may be different.  Enter "Y" to continue with the upgrade.

If the ```apt``` command told you that a restart is required, you can do it like this:

```console
user@introsec:~$ sudo reboot
```

Or you can go back to the "VM Instances" page on [console.cloud.google.com](https://console.cloud.google.com/) ([Direct Link](https://console.cloud.google.com/compute/instances)) and do it from there.

## Installing the Necessary Packages ##

Finally, we'll install some software that we will require for the following labs.  Specifically, we will need:

* ```build-essential``` - For compiling and debugging C programs
* ```gdb``` - For debugging and reverse-engineering C programs
* ```python3-pycryptodome``` - For easy access to crypto primitives in Python
* ```python3-pytest``` - For running our automated test suites

Again we can use ```apt``` tool to install these packages from Ubuntu:

```console
user@introsec:~$ sudo apt install build-essential gdb python3-pycryptodome python3-pytest
```
