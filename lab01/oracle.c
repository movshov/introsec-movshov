#include <stdlib.h>
#include <stdio.h>
#include <string.h> //for strtok.
#include <ctype.h> //for toupper.

//Prototypes
int read_answers(char[], int);

int main(int argc, char *argv[])
{
	char name[200];
	int guess = 0;

	printf("ORACLE: What is your name?\r\n");
	printf("PLAYER: ");
	fgets(name,200,stdin);
	strtok(name, "\n");	//removing new line character. 
	name[0] = toupper(name[0]);	//make sure that the first elelemnt of their name is uppercase. 
	printf("ORACLE: What is the magic number, %s?\r\n", name);
	printf("PLAYER: ");
	scanf("%d", &guess);

	return read_answers(name, guess);

}

int read_answers(char name[], int guess)
{
	FILE * fp; 
	char buff[255];
	char check_name[200];
	int number = 0;
	int check = 0;

	fp = fopen("answers.txt", "r");	//open answers.txt with read permission only. 
	if(fp == NULL) {
		perror("Error opening file\n");
		return -1;
	}
	while(check != EOF){	//While there is still stuff to read keep the file open.
		//puts(buff);	//used for testing.
		fscanf(fp, "%s %d", check_name, &number);
		//printf("check_name is: %s\n", check_name); //used for testing.
		//printf("name is: %s\n", name);
		//printf("number is: %d\n", number);

		if(strcmp(check_name, name) == 0){
			if( number == guess){
				printf("ORACLE: SUCCESS\r\n");
				return 0;
			}
			else if( number > guess){
				printf("ORACLE: TOO LOW\r\n");
				return 1;
			}
			else if( number < guess){
				printf("ORACLE: TOO HIGH\r\n");
				return 1;
			}
		}
	check = getc(fp);
	}
        printf("ORACLE: Magic number not found\r\n");
	fclose(fp);
return -1;
}
