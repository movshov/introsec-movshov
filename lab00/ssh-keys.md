# Generating an SSH Keypair #
SSH provides a convenient and secure mechanism for accessing remote resources.  To save you from having to re-type your password every time, as well as for greater security, we are going to generate cryptographic keypairs that SSH can use to authenticate you to remote services.

The command for generating a new keypair is called ```ssh-keygen```.  Here we're going to ask for a certain type (```-t```) of key, namely, RSA.  We'll talk about what all this means when we get to the unit on Applied Cryptography.  For now, it's enough to paste the following command into a terminal:

```console
$ ssh-keygen -t rsa
```

```ssh-keygen``` will ask you a couple of questions.

* First, where do you want to save this new key?
* And second, what passphrase (if any) would you like to use to encrypt the private half of the key?  It's a good practice to use a passphrase, but be careful: If you forget your passphrase, you won't be able to use the key anymore.  You'll have to generate a new keypair and then spend some time reconfiguring systems to use the new key instead of the old one.

If you're OK with the defaults (save the key into the "hidden" .ssh folder under your $HOME, with no password), you can just hit the Enter key at each question.  Then if all goes well, you should see something that looks like this:

```console
$ ssh-keygen -t rsa
Generating public/private rsa key pair.
Enter file in which to save the key (/home/user/.ssh/id_rsa):
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in /home/user/.ssh/id_rsa.
Your public key has been saved in /home/user/.ssh/foo_rsa.pub.
The key fingerprint is:
SHA256:cE2NNkteIWkdUDBslTZmCyEI6E0vmythE+g5Bj8jGtc user@computer
The key's randomart image is:
+---[RSA 2048]----+
|   ... ...B@=+   |
|  . . .  +@oX    |
| o o .. .*.O o   |
|o o o .o  o .    |
|o..o +  S        |
|o=O E            |
|o=.= .           |
|. . .            |
|   .             |
+----[SHA256]-----+
```

Now let's take a look at what we've created.  You can use the ```cat``` command (short for "conCATenate") to dump the contents of your public key into your terminal.

```console
$ cat .ssh/id_rsa.pub
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDdTGSjYtwpPUs1hSYHmF52zNtlvjUgINFcD0UUbMI8bBAEwaIPN0DpQwRL7hxfDxEjOMIiab7xtiHPyOTzlZqfq5+KJpq6xLg/euXyDp9qkeyeY7Ia0hFeqjgHRHwq8aIo7xxxYI98n8lpOeeFx2mkI7Zyr/SExbfBSZPJBYPP4kk2kJh+eXB5ylxjpJOGnx/eUP393cxlegUr2IpB6htLHg9pTUPP4ZV6Jmd6IjRFGhHsUNs68P/BELmQtkSmKQJ0b8kwol9ebYqrKnJ4ZGPTruVxpVE4I9SEKSMqQcx9lkdwqLbg/3jPWqkyf4RD6vrXkxEq9v0wmjisjQ6L9Vdv user@computer
```

This is important.  You'll need the public key every time you want to configure some remote system for access with this keypair.

## Turning in your public key ##

We also need a copy of your public key(s) for some special infrastructure that we'll be setting up for later labs.  Submit it to this Google form: [Public keys](https://forms.gle/FwoLVAfVSyYgBo9S6).
