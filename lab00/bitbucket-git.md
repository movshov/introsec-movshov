# Creating and Cloning Your Git Repos with Bitbucket #

## Creating Your Account ##
We will be using [BitBucket](https://bitbucket.org/) for hosting our Git repositories in this course.  If you do not have a Bitbucket account, create one now, using your **@pdx.edu** email address.  Using your official PDX email will enable you to use "Sign in with Google" to get into your Bitbucket account without remembering an additional password.


## Adding Your SSH Public Key ##
For convenience and security, we will now add the public key that you created earlier in [Generating an SSH Keypair](ssh-keys.md) to your Bitbucket account.

Sign in to [bitbucket.org](https://bitbucket.org/) and click the icon in the bottom left of the page for "Your profile and settings".  (The icon should show your initials, or if you included a picture in your profile, it will show your profile image.)  In the little pop-up menu, click "**Bitbucket settings**". 

In the Settings menu, under "**SECURITY**", click "**SSH keys**".

On the SSH Keys page, click the button for "**Add key**".  This brings up the dialog for entering a new public key.

* Label: Doesn't really matter.  Pick something descriptive.  e.g. "Introsec VM Spring 2020"
* Key: Here is where you should paste the entire contents of your public key (the .pub file that you created earlier in this lab).

Go ahead and add your public keys for any other systems that you will be using to work on your labs.  For example, if you also generated a public key on your laptop, now is a good time to add it to your Bitbucket account.


## Cloning the Public Repo ##
If you have not yet cloned the ```introsec-public``` Git repository, then you are probably reading these instructions right now from the repo on bitbucket.org.  Our first step will be to get you a local copy (a "clone") of this repo on the system(s) where you'll be doing your lab work.   

You should clone the ```introsec-public``` repo on your new Google Compute Engine VM that you created earlier in this lab.  If you would like to do some work locally on your laptop/desktop machine, we will see how to do that later on this page.

The command that we'll be using to do the clone operation is _almost_ but not quite the same as the one that you'd find if you clicked the "Clone" button on bitbucket.org.  For convenience, our version of the clone command gives the repo a more accurate name ("introsec-labs") on your local system.

```console
user@introsec:~$ git clone git@bitbucket.org:cvwpdx/introsec-public.git introsec-labs
```

## Creating Your Personal Repository ##
In addition to the public repo that you cloned above, you will need to create a repo of your own where you can save your work.  This is also where you will turn in your work for the labs.


Log in to Bitbucket, and you should see a big plus at the left side of the screen.  Click the plus to bring up the menu, and under "CREATE" select "Repository" to bring up the dialog for creating a new repository.  ([Direct Link](https://bitbucket.org/repo/create))

* Owner: You
* Repository Name: Your repo name **must match the following pattern**: Where YOUR_ODIN_ID is your PSU "Odin ID", ie the first "username" part of your pdx.edu email address, your repo name will be "introsec-YOUR_ODIN_ID".  For example, my email address is cvw@pdx.edu, so my personal repo is called introsec-cvw.
* Access Level: This is a private repository (Yes, check the box.)
* Include a README: **No**.  This is important.  If you include a README, you will not be able to push changes from your VM into this repo, and you will need to delete your repository and start over.


## Adding Access for Your Professor and TA ##
In order for us to grade your lab work, we will require access to your repo.

In Bitbucket, navigate to your repo.  (Click the big "bucket" icon at the top left to get to your overview screen.  Then click "**Repositories**" under the Bitbucket menu.  Your new introsec repo should show up in the list.)

On the page for your repo, click "**Settings**" on the left-side menu.

In the Settings menu, click "**User and group access**".  You will need to add access for two users:

* Prof. Wright
    - Email: cvw@pdx.edu
	- Write access
* Anant Pathak (TA)
    - Email: pathak@pdx.edu
	- Write access

	

## Adding Your Personal Repo on Your VM ##
Now we will connect your VM to your new personal Git repo, and we will use the VM to push content into your repo.

Log in to your VM on Google Compute Engine, and navigate to the place where you cloned the ```introsec-public``` repo.  (If you followed the directions, this should be the directory called "introsec-labs" under your home.)

```console
user@introsec:~$ cd introsec-labs
user@introsec:~/introsec-labs$ ls
lab00
lab01
lab02
README.md
```

Now that you're inside your local clone of the Git repo, we can tell Git about your own personal repository on Bitbucket.  The exact content of this command will depend on your PSU Odin ID and your Bitbucket username, but will look like the following.

* Replace YOUR_BITBUCKET_USERNAME with your Bitbucket username
* Replace YOUR_ODIN_ID with your pdx.edu username

```console
user@introsec:~/introsec-labs$ git remote add personal git@bitbucket.org:YOUR_BITBUCKET_USERNAME/introsec-YOUR_ODIN_ID.git
```

Once you've told Git about your Bitbucket repo, you can tell it to push all the content from your local clone up into your personal repo in the cloud.

```console
user@introsec:~/introsec-labs$ git push -u personal master
```

Explanation:

* ```push``` is the command that tells Git to upload data from your local working copy up to a remote repository.
* The ```-u``` flag tells it to push everything.
* ```personal``` is the remote repository where Git should push data to
* ```master``` is the branch.  We will be pulling from the master branch for grading, so be sure to push all your changes to master.

Now if you look at your personal repo on the web at [bitbucket.org](https://bitbucket.org/), you should be able to see that it contains a copy of all the data from the ```introsec-public``` repo.