> Q: Compile and run ```tracer2``` a few times and observe the results.
>
> 1. Where are ```buffer``` and ```array``` relative to stuff?
      Buffer and array are at a higher address than stuff. 

> 2. What happens to the address of ```stuff``` each time the function recurses?
      Stuff's address gets lower each time it recurses. 

> 3. What is going on here? Explain why we get these behaviors in (1) and (2) above.
      In the first question what is happening is that stuff gets initialized before
      buffer and array. So stuff would be lower on the stack than the other two meaning
      that it would have a higher memory address. 

      In the second question because we are recursively looping in the second function, stuff
      gets reinitialized several times. Each time that this happens it gets placed lower on the
      heap each time. This now means that buffer and array are now higher on the heap than stuff. 
