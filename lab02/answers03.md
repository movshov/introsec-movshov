> Compile and run ```tracer3``` and observe the results.
> 
> 1. Each time you run the program, what do all of the code addresses have in common?
      main and function are in the same region of memory while the rest of the variables
      are higher up. 

> 2. Why does (1) happen? Explain what you're seeing.
      The two functions are getting stored in the code/text section of memory while the 
      other variables are stored in the stack or heap sections of memory. 

> 3. Now re-run the program several times. Do the addresses change from run to run? If so, how do they change each time?
      After running the code 5 times the addresses for main and fucntion remain in the same
      region of memory while all of the other variables are different each time. Each time 
      the code is ran buffer, array, and stuff which are all located in the heap are at a 
      different memory address than rc, buff, and x which are all allocated in the stack. 

