#include <stdlib.h>
#include <stdio.h>
#include <string.h> //for strtok.
#include <ctype.h> //for toupper.

int main(int argc, char *argv[])
{
  if(argc < 2){
    printf("Incorrect format. Please use the format: ./player <PLAYER_NAME_HERE>\r\n");
    return -1;
    }
  char * name;
  int magic_number = 0;
  name = argv[1]; //name is given by command line argument.
  //strtok(name, "\n");	//removing new line character. 
  name[0] = toupper(name[0]);	//make sure that the first elelemnt of their name is uppercase. 

  FILE * fp; 
  char buff[255];
  char check_name[200];
  int number = 0;
  int check = 0;

  fp = fopen("guesses.txt", "r");	//open answers.txt with read permission only. 
  if(fp == NULL) {
    perror("Error opening file\n");
    return -1;
  }
  while(check != EOF){	//While there is still stuff to read keep the file open.
    //puts(buff);	//used for testing.
    fscanf(fp, "%s %d", check_name, &number);
    //printf("check_name is: %s\n", check_name); //used for testing.
    //printf("name is: %s\n", name);
    //printf("number is: %d\n", number);

    if(strcmp(check_name, name) == 0){
      magic_number = number;
      printf("%s's number is: %d\r\n",name, magic_number);
      return number;
    }
    check = getc(fp);
  }
  fclose(fp);
  return -1;
}
